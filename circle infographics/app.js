/*global
	 window, TweenMax, jQuery, SlowMo, RoughEase,Power0, SteppedEase, TimelineMax
*/


;(function($) {
	'use strict';
	var mainTween;

	function getPosPropsForAnimation($target) {
		var changeProps = {};

 		if ($target.css('left') !== undefined) {
 			changeProps.left = $target.css('left');
 		}

 		if ($target.css('top') !== undefined) {
 			changeProps.top = $target.css('top');
 		}

 		if ($target.css('right') !== undefined) {
 			changeProps.right = $target.css('right');
 		}

 		if ($target.css('bottom') !== undefined) {
 			changeProps.bottom = $target.css('bottom');
 		}


 		return changeProps;
	}

	function getPlaceClass(classString) {
		var classArray = classString.split(' ');
		var placeClass;
		classArray.forEach(function(item){
			if (item.indexOf('place')===0) {
				placeClass = item;
			}
		});
		return placeClass;
	}

 	$(document).on('click', '.box', function(){
 		if (mainTween !== undefined) {
 			console.log('progres - ', mainTween.isActive());
 		}

 		if ((mainTween === undefined)||(mainTween.isActive() === false)){
	 		var $newupbox = $(this);
	 		var $prevupbox = $('.box.place12');

	 		//var currentclass = $(this).prop('class').replace('box','').replace(' ','');
	 		var currentclass = getPlaceClass($(this).prop('class'));

	 		//console.log('changing ',$prevupbox);

	 		var changePropsPrev = getPosPropsForAnimation($newupbox); 		
	 		changePropsPrev.onComplete = function(){
	 			$prevupbox.removeClass('place12').addClass(currentclass);
	 			$prevupbox.children('p').css('opacity',1);
	 		};
	 		changePropsPrev.backgroundPosition = '50% 50%';

	 		var changePropsNew = getPosPropsForAnimation($prevupbox);
	 		changePropsNew.onComplete = function(){
	 			$newupbox.removeClass(currentclass).addClass('place12');
	 		};	
	 		changePropsNew.backgroundPosition = '50% 0';	

	 		mainTween = new TimelineMax ()
	 					.add([
	 						TweenMax.to($prevupbox.children('p'), 0.25, {opacity:0}),
	 						TweenMax.to($prevupbox, 1, changePropsPrev),
	 						TweenMax.to($newupbox, 1, changePropsNew)
	 					]);
	 	}
 	});

}(jQuery));