/*global
	jQuery, $, window, createjs
*/

;(function( $ ) {
	'use strict';

	$(document).ready(function(){
		console.log(new Date().toLocaleTimeString() + ' - starting');
		var params = {
			rect1 : {velocity: { fill: '#8d5524'}, other:{delay : 500, duration: 4500}},
			roof : {velocity: { fill: '#555'}, other:{delay : 500, duration: 4500}}, 
			tower : {velocity: { fill: '#c68642'}, other:{delay : 500, duration: 4500}},  
			windows : {velocity: { fill: '#e0ac69'}, other:{delay : 500, duration: 3500}},
			inner_windows :  {velocity: {stroke : "#000080", fill : "#5fbcd3", 'stroke-dashoffset':1600}, other:{delay : 500, duration: 3500}},  
			door1 : {velocity: {fill : "#a05a2c"}, other:{delay : 500, duration: 3500}},
			door2 : {velocity: {stroke : "#714335", fill : "#d38d5f", 'stroke-dashoffset':500}, other:{delay : 1500, duration: 3500}},
			tower_windows :  {velocity: {stroke : "#803300", fill : "#5fbcd3", 'stroke-dashoffset':400},other:{delay : 2000, duration: 3500}},
			clockback : {velocity: {stroke : "#803300", fill : "#520", 'stroke-dashoffset':500},other:{delay : 2000, duration: 3500}},
			clockend1 : {velocity: {stroke : "#000", 'stroke-dashoffset':200},other:{delay : 3000, duration: 3500}},
			clockend2 : {velocity: {stroke : "#000", 'stroke-dashoffset':200},other:{delay : 3500, duration: 3500}},
			cloccircle1: {velocity: {stroke : "#000", fill: "#fca" , 'stroke-dashoffset':1000 },other:{delay : 2000, duration: 3500}},
			cloccircle2: {velocity: {stroke : "#000", fill: "#fca"},other:{delay : 2000, duration: 3500}},
			clockarrows : {velocity: {fill: "#241c1c"},other:{delay : 2000, duration: 3500}},
			clockcenter: {velocity: {stroke : "#000", fill: "#241c1c"},other:{delay : 2000, duration: 3500}},
			roof2 : {velocity: { fill: '#555'}, other:{delay : 500, duration: 4500}},
			roofpaint : {velocity: { fill: '#343d46'}, other:{delay : 500, duration: 4500}},
			roofpaint2 : {velocity: { fill: '#ffdbac'}, other:{delay : 500, duration: 4500}},
			roofpaint3 : {velocity: { fill: '#a47c48'}, other:{delay : 500, duration: 4500}},
			roofpaint4 : {velocity: { fill: '#ffdbac'}, other:{delay : 500, duration: 4500}}
		};

		$('.path').each(function(){
			var $item = $(this);
			
			if (params.hasOwnProperty(this.id)) {

			//console.log('$item.id - ', this.id, params[this.id]);

				if (params[this.id].velocity.hasOwnProperty('stroke-dashoffset')) {
					$item.velocity({ 'stroke-dashoffset': params[this.id].velocity['stroke-dashoffset'], 
									 'stroke-dasharray': params[this.id].velocity['stroke-dashoffset'] ,
									  stroke : params[this.id].velocity.stroke }, 0);
					params[this.id].velocity['stroke-dashoffset'] = 0;
				}
				
				$item.velocity(params[this.id].velocity, params[this.id].other);
			}
		}); 
	});
}(jQuery));	 