;(function(){

	d3.simpleBarChat = function(params) {

		var margin = 20;
		var barOpacity = 0.3;
		var xLabelsMargin = '1.2em';

		var defaultGrClass = 'defaultGradient';
   		var defaultColorClass = 'defaultColor';

   		/*********/
   		var heightAll, height, width, widthAll;

   		var maxAssesment, minAssesment;

   		var widthBar, heightCap;

   		var xScaleLabels = [], yScaleLabels = [];

   		var chatBlock, chatSVG, xScale, yScale;

   		var currentDataArray;
   		/*********/

   		function prepareScales() {
   			var formatMonth = d3.timeFormat("%b %Y");
   			xScaleLabels = [];
   			currentDataArray.forEach(function(item){
			
				var dt = new Date(item.month.substr(0,4), item.month.substr(5,2)-1);
				xScaleLabels.push(formatMonth(dt));
			});

   			yScaleLabels = params.yScaleLabels;
			if (d3.min(yScaleLabels)>0) {
		    	yScaleLabels.unshift(0);
		    }
			if (maxAssesment > d3.max(yScaleLabels)) {
		    	yScaleLabels.push(maxAssesment*1.1);
		    }
   		}

   		function createMainRects() {
   			var mainRects = chatSVG
		    	.append('g')
		    		.classed('mainRects', true)
		    		.attr("transform", "translate(" + margin + ",0)");

	    	mainRects
		        .selectAll("rect")

		        .data(currentDataArray)
		        .enter()

		        .append("rect")
		          .attr("width", widthBar*.6)
		          .attr("height", 0)
		          .attr("x", function(d, i) { return i * widthBar; })
		          .attr("y", height)

		          .style("fill", function(d) {

		          	var resColor = "url(#" + defaultGrClass + ')';

		          	params.colorsArray.forEach(function(item) {
		          		if (d.assesment> item.min && d.assesment<=item.max) {
		          			resColor = "url(#" + item.classGr + ')';
		          		}
		          	});

		          	return resColor;

		          })
		          .style('opacity', barOpacity)
		          .transition()
		          	.duration(params.animationDuration)
	    			.ease(d3.easeLinear)
		          	.attr("height", function(d) { return height - yScale(d.assesment); })
		          	.attr("y", function(d, i) { return yScale(d.assesment); })
		          ;		    		
   		}

	   	function addCapsToChart() {
	   		var heightCap = height * .03;
		    chatSVG
		    	.append('g').classed('capRects', true)
		    	.attr("transform", "translate(" + margin + ",0)")
		        .selectAll("rect")
		        .data(currentDataArray)
		        .enter()
		        //top cap rect
		        .append("rect")

		          .attr("width", widthBar*.6)
		          .attr("height", function(d,i) { if (d.assesment>0) { return heightCap; } else { return 0;} })
		          .attr("x", function(d, i) { return i * widthBar; })
		          .attr("y", function(d) { return 0;})
		          .attr("class", function(d) {

		          	var classColor = defaultColorClass;
		          	params.colorsArray.forEach(function(item) {
		          		if (d.assesment> item.min && d.assesment<=item.max) {
		          			classColor = item.classCol;
		          		}
		          	});

		          	return classColor;

		          })
		          .style('opacity', 0)
		          .transition()
		          	.duration(params.animationDuration*.8)
		          	.attr("y", function(d) { return yScale(d.assesment);})
		          	.style('opacity', 1);
	   	}   	

	   	function addAxis() {
		    var xAxis = d3.axisBottom(xScale)
		    				.tickPadding(10)
		    				.tickFormat(function(d) { 
		    					return xScaleLabels[d]; })
		    				;

		    chatSVG.append("g")
		    	.attr('class', 'x-axis')
	          	.attr("transform", function() {
	            	return "translate(" + margin + "," + height + ")"
	          	})
	          	.call(xAxis)
	          	.selectAll('.x-axis .tick text') // select all the x tick texts
					          .call(function(t){                
					            t.each(function(d){ // for each one
					              var self = d3.select(this);
					              var s = self.text().split(' ');  // get the text and split it
					              self.text(''); // clear it out
					              self.append("tspan") // insert two tspans
					                .attr("x", widthBar*.3)
					                .attr("dy",xLabelsMargin)
					                .text(s[0]);
					              self.append("tspan")
					                .attr("x", widthBar*.3)
					                .attr("dy",xLabelsMargin)
					                .text(s[1]);
					            })
					        });

			var yAxis = d3.axisLeft(yScale)
		    				.tickPadding(5)
		    				.tickValues(yScaleLabels)
						;

		   	chatSVG.append("g")
		    	.attr('class', 'y-axis')
		    	.attr("transform", "translate(" + margin + ",0)")
	          	.call(yAxis);

	       chatSVG.selectAll("g.y-axis g.tick")
	        	.append("line")
				.classed("grid-line", true)
				.attr("x1", 0)
				.attr("y1", 0)
				.attr("x2", width-margin*1.5)
				.attr("y2", 0);

			chatSVG.append("g")
		    	.attr('class', 'y-axis')
		    	.attr("transform", "translate(" + (width+margin*1.5) + ",0)")
	          	.call(yAxis);
	   	}	   		

		function draw(dataArray) {

			var barChatDom = document.getElementById(params.barChatID);
			currentDataArray = dataArray;

			heightAll = barChatDom.offsetHeight;
	    	height = heightAll - margin*3;

	    	widthAll = barChatDom.offsetWidth;
	    	width = widthAll-margin*2;

	    	maxAssesment = d3.max(dataArray, function(d, i) { return d.assesment});
			minAssesment = d3.min(dataArray, function(d, i) { return d.assesment});

			widthBar = width / dataArray.length;
			
			prepareScales();

			chatBlock = d3.selectAll('#'+params.barChatID);
			chatBlock.selectAll("*").remove();

			chatSVG = chatBlock.append('svg')
						.attr('height', heightAll)
						.attr('width', widthAll)
						.style("padding", "10px");

			xScale = d3.scaleLinear()
	    			.domain([0, currentDataArray.length])
          			.range([0, width]);

	    	yScale = d3.scaleLinear()
	                     .domain([maxAssesment*1.1, 0])
	                     .range([0, height]);

	        createMainRects();

	       	setTimeout(function() {
	    		addCapsToChart();
		    }, params.animationDuration);

		    addAxis();
		}

		return {
			draw : draw
		}
	}

}());


;(function(){

	d3.createGradientsSVG = function(colorsArray) {

		var gradientsClasses=['defaultGradient'];

		function initGradientsClasses() {

			colorsArray.forEach(function(item){
				if (gradientsClasses.indexOf(item.classGr) === -1) {
					gradientsClasses.push(item.classGr);
				}
			});

		}

		function create() {

			initGradientsClasses();

			var gradientsSvg = d3.select('body').append('svg')
				.attr('height', 0)
				.attr('width', 0);

   			var svgDefs = gradientsSvg.append('defs');   			
   		
   			gradientsClasses.forEach(function(curGradient) {
	   			var gradObj = svgDefs.append('linearGradient')
					   			.attr('id', curGradient)
					            .attr("x1", "0%").attr("y1","0%")
					            .attr("x2", "0%").attr("y2","60%")
					            .attr("x2", "0%").attr("y2","100%")
					            .attr("spreadMethod","pad")
					            .classed(curGradient, true);

				gradObj.append('stop')
	                .attr('class', 'stop-top')
	                .attr('offset', '0');

	            gradObj.append('stop')
	                .attr('class', 'stop-middle')
	                .attr('offset', '0.6');

	        	gradObj.append('stop')
	                .attr('class', 'stop-bottom')
	                .attr('offset', '1');
   			}); 
		}
		return {
			create: create
		}
	}

}());

;(function(){

	d3.dataTableForGraph = function(params) {
		
		var currentDataArray = params.dataArray;
		
		function init() {
	   		var dataTableBlock = d3.selectAll('#'+params.dataTableID);

	   		var tbl = dataTableBlock.append('table').classed('table', true);

	   		tbl
	   			.append('thead')
	   			.append('tr')
	   				.selectAll('td')
	   				.data(currentDataArray)
	   				.enter()
	   				.append('td')
	   					.text(function(d) { return d.month;});

	   		tbl
	   			.append('thead')
	   			.append('tr')
	   				.selectAll('td')
	   				.data(currentDataArray)
	   				.enter()
	   				.append('td')
	   					.html(function(d, i) { return '<input type="text" class="form-control" id="assesment'+ i +'" value="' + d.assesment + '">' ;});
		}

		function recalcDataArray(){
			var dataTableBlock = d3.selectAll('#'+params.dataTableID);
			var inputs = dataTableBlock.selectAll('table tr td input');

			if (inputs.nodes().length>0) {
				inputs.nodes().forEach(function(item) {
					var indexData = parseInt(item.id.replace('assesment',''));
					var newValue = parseFloat(item.value);

					currentDataArray[indexData].assesment = newValue;
				});
			}

			return currentDataArray;
		}

		return {
			init: init,
			recalcDataArray : recalcDataArray
		}
	}
}());