;(function(){
	
	var dataArray = [{month: '2017/01', assesment: 2}, {month: '2017/02', assesment: 3.9}, {month: '2017/03', assesment: 2.4}, {month: '2017/04', assesment: 1.25},
   				 {month: '2017/05', assesment: 3.12}, {month: '2017/06', assesment: 1.47}, {month: '2017/07', assesment: 3.56}, {month: '2017/08', assesment: 2.55},
   				 {month: '2017/09', assesment: 0.88}, {month: '2017/10', assesment: 0}, {month: '2017/11', assesment: 0}, {month: '2017/12', assesment: 0}];

   	var colorsArray = [{ min:0, max:2.5, classGr: 'gradient1', classCol: 'color1'}, { min:2.5, max:3.25, classGr: 'gradient2', classCol: 'color2'}, { min:3.25, max:10, classGr: 'gradient3', classCol: 'color3'}];

   	d3.createGradientsSVG(colorsArray).create();

	var barChatPlugin = d3.simpleBarChat({
		colorsArray: colorsArray,
		barChatID: 'barChat',
		animationDuration : 500,
		yScaleLabels : [1, 1.75, 2.5, 3.25, 4]
	});

	barChatPlugin.draw(dataArray);

	var dataTablePlugin = d3.dataTableForGraph({
		dataArray : dataArray,
		dataTableID: 'dataTable'
	});

	dataTablePlugin.init();

	var resizeTimer;
	window.onresize = function(event) {
	    clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {

		    barChatPlugin.draw(dataArray);
		            
		}, 250);
	};

	document.getElementById("redrawButton").onclick = function(event) {
		
		dataArray = dataTablePlugin.recalcDataArray();
		barChatPlugin.draw(dataArray);

	}

}());