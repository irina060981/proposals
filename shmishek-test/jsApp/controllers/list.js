/*global
	 angular
*/

;(function(){
	'use strict';
    angular
        .module("acentQuizApp")
        .controller("listCtrl", ListController);

    ListController.$inject = ['quizMetrics', 'DataService'];

    function ListController(quizMetrics, DataService){
        var self = this;
        self.data = DataService.personsData;

        //console.log('DataService - ', DataService);

        self.activePerson = {}; 
        self.changeActivePerson = changeActivePerson; 

        self.search = "";

        //self.quizActive = false;
        self.activateQuiz = activateQuiz;
        self.quizMetrics = quizMetrics;

        function changeActivePerson(index){
            self.activePerson = index;
        }

        function activateQuiz(){
            //self.quizActive = true;
            //console.log('activated');
            quizMetrics.changeState('quiz', true);
        }
    }



}());
