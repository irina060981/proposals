/*global
	 angular, jQuery, $
*/

;(function($){
	'use strict';
    angular
        .module("acentQuizApp")
        .controller("resultsCtrl", ResultsController);

    ResultsController.$inject = ['quizMetrics', 'DataService'];

    function ResultsController(quizMetrics, dataService){
       
        var self = this;
        

        self.quizMetrics = quizMetrics;
        self.dataService = dataService;
        self.printResults = [];
        
    }

}(jQuery));