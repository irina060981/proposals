/*global
	 angular, jQuery, $
*/

;(function($){
	'use strict';
    angular
        .module("acentQuizApp")
        .controller("quizCtrl", QuizController);

    QuizController.$inject = ['quizMetrics', 'DataService'];

    function QuizController(quizMetrics, dataService){
        var self = this;
        //var finalAnswer = {};

        self.questionCntInGroup=10;


        self.quizMetrics = quizMetrics;
        self.dataService = dataService;

        self.cntGroups = Math.ceil(dataService.quizQuestions.length/self.questionCntInGroup);
        

        self.questionsGroup = createUpGroups(self.cntGroups, self.questionCntInGroup, dataService.quizQuestions.length);
        self.selectedGroup = 0;

        self.chosenQuestions = dataService.quizQuestions.slice(0,self.questionCntInGroup);
        self.givenAnswers = 0;
        self.checkGivenAnswers = self.chosenQuestions.length;
        self.answers = {};

        self.finalButtonName = false;

        self.questionGroupAnswered = function(){
            var maxQuestionNum;

            calcAnswers(self.answers, dataService.quizQuestions);

            self.selectedGroup +=1;

            if  (self.selectedGroup===self.cntGroups) {
                //console.log('finished!', finalAnswer);
                 quizMetrics.preparePrintResult(dataService.personsData);
                quizMetrics.changeState('quiz', false);
                quizMetrics.changeState('results', true);
            }
            else {
                maxQuestionNum = self.questionCntInGroup*(self.selectedGroup+1);
                if (maxQuestionNum > dataService.quizQuestions.length) {
                    self.finalButtonName = true;
                }

                self.chosenQuestions = dataService.quizQuestions.slice(self.questionCntInGroup*self.selectedGroup, self.questionCntInGroup*(self.selectedGroup+1));
                self.givenAnswers = 0;
                self.checkGivenAnswers = self.chosenQuestions.length;
                self.answers = {};

                $('html, body').animate({
                  scrollTop: 0
                }, 1000);
            }
        };       

        self.clickAnswer = function(event) {
            //console.log('clicked ',$(event.target.previousSibling));
            var $inputChosen = $(event.target.previousSibling);
            if ($inputChosen.prop('name') !== undefined) {
                $inputChosen.prop('checked', true);
                self.answers[$inputChosen.prop('name').replace('question','')]=$inputChosen.val();

                self.givenAnswers = Object.keys(self.answers).length;
            }
        };

        function calcAnswers(givenAnswers, allanswers) {
            var currentAnswer;

            Object.keys(givenAnswers).forEach(function(item){
                //onsole.log('item - ', allanswers[item-1].answers);

                Object.keys(allanswers[item-1].answers).forEach(function(inner){
                    if (quizMetrics.finalResult[inner] === undefined) {
                        quizMetrics.finalResult[inner] = 0;
                    }
                    currentAnswer = allanswers[item-1].answers[inner];
                    if (currentAnswer.type ===  parseInt(givenAnswers[item-1]) ) {
                        quizMetrics.finalResult[inner] += currentAnswer.koef;   
                    }
                    
                });
            });


        }

        function createUpGroups(cntGroups, cntInGroup, allCntQuestions){
            var questionsGroups = []; 
            var i;
            for (i = 1; i <= cntGroups; i+=1) {
               if (i < cntGroups) {
                    questionsGroups.push(i*cntInGroup); 
               }
               else {
                    questionsGroups.push(allCntQuestions);
               }
            }
            return questionsGroups;
        }

        
    }

}(jQuery));