/*global
	 angular
*/

;(function(){
	'use strict';
    angular
        .module("acentQuizApp")
        .factory("quizMetrics", QuizMetrics);

    function QuizMetrics(){
        var quizObj = {
                quizActive: false,
                changeState: changeState,
                resultsActive: false,
                finalResult:{},
                printResults:[],
                preparePrintResult: preparePrintResult
            };

        return quizObj;

        function changeState(metric, state){
            if(metric === "quiz"){
                quizObj.quizActive = state;
            }else if(metric === "results"){
                quizObj.resultsActive = state;
            }else{
                return false;
            }
        }

        function preparePrintResult(personsData){
            //console.log('first - ', quizObj.finalResult);

            personsData.forEach(function(itemPerson){
                var printItem;

                //console.log(itemPerson.id, quizObj.finalResult[itemPerson.id] );

                if (quizObj.finalResult[itemPerson.id]>14) {
                    printItem = itemPerson;
                    printItem.result = quizObj.finalResult[itemPerson.id];
                    quizObj.printResults.push(printItem);
                }

            });

            console.log('res - ', quizObj.printResults);
        }
    }

}());